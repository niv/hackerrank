# Enter your code here. Read input from STDIN. Print output to STDOUT
def same_sign(x,y):
	if x<=0 and y<=0:
		return True
	if x>=0 and y>=0:
		return True
	return False

def get_sum_cont_subarray(arr):
	
	# sum all positives and negatives seperatly
	opposites_arr = [arr[0]]
	last = 0 # last index in new_arr
	for i in range(1, len(arr)):
		if same_sign(arr[i], opposites_arr[last]):
			opposites_arr[last] = opposites_arr[last] + arr[i]
		else:
			opposites_arr.append(arr[i])
			last = last + 1
			
	if opposites_arr[0] < 0: # remove first element if its a negative since it wont contribute for best sum
		del opposites_arr[0]
		
	if len(opposites_arr) == 0: # if we left with no elements, means all elements are negative, thus we'll seek smallest one for smallest sum
		return max(arr)
	
	if len(opposites_arr) == 1: # only one positive elements, means all are positive, return that value
		return opposites_arr[0]
		
	#print opposites_arr
	# now we'll work on our new array, for convinience we'll call it "arr"
	arr =  opposites_arr
	
	compressed_arr = [arr[0]] # will contain compressed array, negatives and positives will be combined if they give better result, thus the answer will be in this final array as the largest number in here
	last = 0
	for i in range(1, len(arr)):
		#print i, compressed_arr
		sum_with_last = arr[i] + compressed_arr[last]
		
		
		
		if arr[i] < 0 or (arr[i]>0 and compressed_arr[last]>0):
			compressed_arr.append(sum_with_last)	
		else:
			compressed_arr.append(arr[i])
		last = last + 1			
				
		#print compressed_arr
	#print compressed_arr
	best_sum = max(compressed_arr)
	
	return best_sum


def get_sum_cont_subarray_naive(arr):
	
	# sum all positives and negatives
	new_arr = [arr[0]]
	last = 0 # last index in new_arr
	for i in range(1, len(arr)):
		if same_sign(arr[i], new_arr[last]):
			new_arr[last] = new_arr[last] + arr[i]
		else:
			new_arr.append(arr[i])
			last = last + 1

	if len(new_arr) != 1 or new_arr[0] > 0:
		arr =  new_arr
	
	best_sum = arr[0]
	
	for i in range(len(arr)):
		# calculate the best sum for starting with index i
		best_i_sum = arr[i]
		cur_i_sum = arr[i]
		
		for j in range(i+1, len(arr)):
			cur_i_sum = cur_i_sum + arr[j]
			if cur_i_sum > best_i_sum:
				best_i_sum = cur_i_sum
				
		if best_i_sum > best_sum:
			best_sum = best_i_sum
	
	
	return best_sum


def get_array_input(inp = None):
    if inp == None:
        return raw_input()
    else:
        return inp

def get_highest_sum(arr):
	sum = 0
	closest_to_zero = arr[0]
	for x in arr:
		if x > 0:
			sum = sum + x
		if x < 0 and x > closest_to_zero:
			closest_to_zero = x
	if sum == 0:
		return closest_to_zero
	return sum

def solve_dynprog_maxsubarray(N):
    for i in range(N):
        input() # len
        # input string into an array
        arr = map(int, get_array_input().strip().split(" "))
        print get_sum_cont_subarray(arr),get_highest_sum(arr)

N = input()
solve_dynprog_maxsubarray(N)