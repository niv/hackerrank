M=input()
N=input()
mat=[map(int, raw_input().split()) for _ in range(M)]

def flood(row, col):
    # check boundaries and if element wasn't visited before
    if (row>-1 and col>-1 and row<M and col<N) and mat[row][col]==1:
        mat[row][col] += 1 # could also set it to any other number other than 1
        flood_rows = flood(row,col+1)+flood(row,col-1)
        flood_cols = flood(row+1,col)+flood(row-1,col)
        diag_1 = flood(row+1,col+1)+flood(row-1,col-1)
        diag_2 = flood(row+1,col-1)+flood(row-1,col+1)
        return 1+flood_rows+flood_cols+diag_1+diag_2
    return 0


max_area = 0
for row in range(M):
    for col in range(N):
        if mat[row][col]==1:
            cur_area = flood(row, col)
            if cur_area > max_area:
                max_area = cur_area

print max_area

