# Enter your code here. Read input from STDIN. Print output to STDOUT
	
from collections import defaultdict
from copy import deepcopy

class Node:
	def __init__(self, id):
		self.id = id
		
class Graph:
	"""
	Graph/Tree represented by a dictionary;
	keys	= integer type, unique IDs of the node
	vals	= tuple (mark, [node1, node2, ...])
		where mark is a mark of that key-node which could be used for flooding the tree and traversing it 
		and the list of nodes connected to they key-node
	"""
	_INIT_MARK = 0 # starting value of the nodes special mark
	
	def __init__(self, orig=None):
		self.mark = Graph._INIT_MARK+1 # normal mark, used to flood the tree. only nodes not marked by the value of self.mark will be checked (to indicate we already visit those nodes)
		if orig!=None: # then copy-ctor
			self.nodes = deepcopy(orig.nodes)
		else:
			self.nodes = {} # nodes as keys, node-list representing edges to that key node
		
	# return the mark on a given node ID
	def getNodeMark(self, id):
		return self.nodes[id][0]
		
	# return the connections list to a given node ID
	def getNodeList(self, id):
		return self.nodes[id][1]
	
	# set the node mark to the graph self.mark value
	def setNodeMark(self, id):
		self.nodes[id] = (self.mark, self.getNodeList(id))
		
	# connect two nodes to each other
	def connect(self, id1, id2):
		self.updateSingle(id1, id2)
		self.updateSingle(id2, id1)
		
	def unconnect(self, id1, id2):
		self.nodes[id1][1].remove(id2)
		self.nodes[id2][1].remove(id1)
	
	def updateSingle(self, key, val):
		if key in self.nodes:
			self.nodes[key][1].append(val)
		else:
			self.nodes[key] = (Graph._INIT_MARK, [val])
			
	def remove(self, id):
		removed = self.nodes[id][1]
		for node in removed:
			self.unconnect(id,node)
		return removed
	
	
	# flood the graph from given root, return list of all nodes we've got
	# (assuming no cycles?)
	def floodFrom(self, root, funOnNode = None, funOnEdge = None):
		self.setNodeMark(root) # making the node having the current graph mark
		children = self.getNodeList(root)
		# a function on the node itself
		if funOnNode != None:
			funOnNode(self, root)
		for node in children:
			if self.getNodeMark(node) == Graph._INIT_MARK: # (!!!) only flooding nodes which mark is zero meaning we didnt pass on them yet				
				# a function on every edge
				if funOnEdge != None:
					funOnEdge(self, (root, node))
				# recursive flooding from children
				self.floodFrom(node, funOnNode, funOnEdge)	
	DFS = floodFrom
	
	# flood the whole graph with marks to indicate different sub-graphs or sub-trees.  giving starting mark is optional, otherwise the graph mark is taken and increment from there
	# NOTE: assuming all marks are 0 before starting
	# NOTE: this is basically DFS
	def flood(self, starting_mark = None, funOnNode = None, funOnEdge = None, with_reset = True):
		if with_reset:
			self.resetMarks()
	
		if starting_mark == None:
			starting_mark = self.mark
		
		for key in self.nodes: # flooding all nodes
			if self.getNodeMark(key) == Graph._INIT_MARK: # skip nodes that were already flooded
				self.floodFrom(key, funOnNode, funOnEdge)
				self.mark += 1 # giving unique mark to different trees in the forest
	DFS_Graph = flood
	
	# reset all nodes mark value to Graph._INIT_MARK
	# reset graph mark to Graph._INIT_MARK+1
	def resetMarks(self):
		self.mark = Graph._INIT_MARK # setNodeMark will set nodes mark to this value
		for node in self.nodes:
			self.setNodeMark(node)
		self.mark = Graph._INIT_MARK + 1
		
		
	# return the amount of (unique) nodes of a tree given by root inside the graph
	def countTreeNodes(self, root, with_reset = False):
		node_counter = [0] # i need a pointer for this to work
		if with_reset:
			self.resetMarks()
		def count_nodes(_, node): node_counter[0] += 1
		self.DFS(root, funOnNode = count_nodes)
		return node_counter[0]
		
		
	def getEdges(self, root = None):
		my_edges = []
		def add_to_list (_, (x,y)): my_edges.append((x,y))
		self.DFS_Graph(funOnEdge = add_to_list) 
		return my_edges
	
	
	def show(self, with_marks = False):
		if with_marks:
			print self.nodes
		else:
			for key, value in self.nodes.iteritems():
				print key, value[1], ",",
			print ""
		
def getExpanded(orig, sub, from_key):
	return {key: value for (key, value) in orig.iteritems() if key in orig[from_key]}

def getATree(graph):
	d = graph.nodes.copy()
	sub = {key: value for (key, value) in d.iteritems() if key==3}
	prev_size = -1
	while prev_size != len(sub):
		prev_size = len(sub)
		for subkey in sub:
			sub = dict(sub.items()+ getExpanded(d, sub, subkey).items())
	print sub
	

		

def getForest(orig_graph):
	forest = [] # list of dictionaries
	graph = Graph(orig_graph)
	
	graph_nodes = graph.nodes
	graph_keys = graph_nodes.keys()
	
	for key in graph_keys: # while not empty
		connected_nodes = graph_nodes[key]
		while connected_nodes: # while not empty
			node = connected_nodes[0]
			graph.unconnect(key,node)
		graph_nodes.pop(key)
		print graph.nodes
	
	return forest
	#for node in graph.nodes:
		

def ch_EvenTree(graph):		
	count_removed_edges = 0
	edges = graph.getEdges()
	for (x,y) in edges:
		# remove the edge, check if the graph still work now, and if not then bring the edge back
		graph.unconnect(x,y)
		graph.resetMarks()
		if (graph.countTreeNodes(x) % 2 == 0) and (graph.countTreeNodes(y) % 2 == 0): # assuming no circles, so the marks dont overlap
			count_removed_edges += 1
		else:
			graph.connect(x,y)
	
	print count_removed_edges

    
if __name__ == "__main__":    
    [amount_vertices,amount_edges] = [int(i) for i in raw_input().strip().split()]
    graph = Graph()
    while amount_edges > 0:
        [node1, node2] = [int(i) for i in raw_input().strip().split()]
        graph.connect(node1,node2)
        amount_edges -= 1
        #print node1,node2
    ch_EvenTree(graph)