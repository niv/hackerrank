# variables

tree = {}# vertices as keys, and list of vertices as values
edges = [] # just a list of the edges
weights = {} # vertices as keys, weight as values
visited = {} # vertices are keys, integers as values indicating what generation of traverse we are doing on the tree
visited_generation = 1

#-------------------------------------------------------#
# tree API

def add_edge(v1, v2, redo=False):
    tree[v1].append(v2)
    tree[v2].append(v1)
    if not redo:
        v1,v2 = (v1,v2) if v1<v2 else (v2,v1)
        edges.append((v1,v2))
    pass

def add_vertice(v, w):
    tree[v] = []
    weights[v] = w
    visited[v] = visited_generation - 1

def remove_edge(v1, v2, temporary=False):
    tree[v1].remove(v2)
    tree[v2].remove(v1)
    if not temporary:
        v1,v2 = (v1,v2) if v1<v2 else (v2,v1)
        edges.remove((v1,v2))
        
    
    
#-------------------------------------------------------#
# input and tree constructions

N = input()

# input weights and initialize nodes
i = 1
for weight in map(int,raw_input().split()):
    add_vertice(i, weight)
    i+=1

# input edges
for _ in range(N-1):
    v1, v2 = map(int,raw_input().split())
    add_edge(v1,v2)
    

#-------------------------------------------------------#
# 

# BFS algorithem
def BFS(vertice, handle, args=None, return_handle=None):    
    global visited_generation
    
    queue = [vertice]
    while queue: # while not empty
        v = queue.pop(0)
        if visited[v] != visited_generation:
            visited[v] = visited_generation
            handle(v, args)
            queue.extend(tree[v]) # append to the queue the direct neighbours of the popped vertice
                
    # when we finished traversing, increasing the generation, so next BFS will be anew
    visited_generation += 1
    if return_handle:
        return return_handle(args)


#-------------------------------------------------------#
class summer_class:
    def __init__(self):
        self.sum = 0
    def add(self, x):
        self.sum += x
    def get(self):
        return self.sum
    
def sum_weights(vertice, s): 
    s.add(weights[vertice])
    pass

def sum_returned(s):
    return s.get()

#-------------------------------------------------------#

best_delta = 1+1001*10**5
def naive_algo():
    global best_delta
        
    for (v1,v2) in edges:    
        remove_edge(v1,v2,True)
        
        sum1 = BFS(v1, sum_weights, summer_class(), sum_returned)
        sum2 = BFS(v2, sum_weights, summer_class(), sum_returned)

        delta = sum2-sum1 if sum1<sum2 else sum1-sum2
        best_delta = delta if delta<best_delta else best_delta

        add_edge(v1,v2,True)
        
    print best_delta
    pass

naive_algo()