
# NOTE: in our case the pivot will be the last element but we can ofc make it random and just swap it with the last element and do everything the same
def quicksort_inplace(arr, lo=0, hi=0):
    # partition return the index which our pivot should be swapped with
    def partition(lo, hi): # LOw and HIgh indices of the array that we gonna check
        pivot, pivot_val = hi, arr[hi]
        cur = lo
        result = hi # where we gonna put our pivot
        hi -= 1
        while cur<=hi and arr[cur] <= pivot_val: # skipping all smaller elements
            cur+=1
        # now either cur points to bigger value than pivot or its the end of array
        if cur==hi:
            return hi # gonna swap it with itself since all elements are smaller than pivot
        # else
        result = cur # here we'll place the pivot at the end. increases as we use the swap algorithem
        cur+=1
        while cur <= hi:
            if arr[cur] <= pivot_val:
                # swap
                arr[cur], arr[result] = arr[result], arr[cur]
                result += 1
            cur += 1
        return result

    def quicksort(lo=0, hi=len(arr)-1):
        if lo>=hi:
            return
        pivot_to = partition(lo,hi)
        # swap pivot to its right position
        arr[pivot_to], arr[hi] = arr[hi], arr[pivot_to]
        # print after each partition
        
        for num in arr:
            print num,
        print
        # recursive
        quicksort(lo, pivot_to-1)
        quicksort(pivot_to+1, hi)

    return quicksort()

if __name__ == "__main__":
    garbage = raw_input()
    arr = map(lambda x: int(x), raw_input().split(' '))
    quicksort_inplace(arr)