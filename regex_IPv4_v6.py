import re

# IPv4 octet definition
p255 = r"""
(
\d          |
[1-9]\d     |
1\d\d       |
2[0-4]\d    |
25[0-5]
)
"""
# now the actual pattern, consists of 4 octets delimeted by dot
st=r'(^%s\.%s\.%s\.%s$)' % (p255,p255,p255,p255)
pattern_ipv4 = re.compile(st, re.VERBOSE)

# IPv6... double-octet definition
pv6 = r"""
(
[a-f0-9]{0,4}
)
"""
st2=r'(^%s:%s:%s:%s:%s:%s:%s:%s$)' % (pv6,pv6,pv6,pv6,pv6,pv6,pv6,pv6)
pattern_ipv6 = re.compile(st2, re.VERBOSE)

def check(s):
    if pattern_ipv4.match(s):
        return 'IPv4'
    if pattern_ipv6.match(s):
        return 'IPv6'
    return "Neither"

for _ in range(input()):
    s = raw_input()
    print check(s)