#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#define SIZE_T 100000
#define SIZE_NUM 10000000000
#define SIZE_FIB_ARR 51 // the 50th element is bigger than 10^10
//#define NUM_TYPE long long
#define INP_TYPE long
#define INP_SCANF "%lu"
#define NUM_TYPE long
#define NUM_SCANF "%lu"
//#define NUM_SCANF "%llu"
#define PRINT_YES printf("IsFibo\n")
#define PRINT_NO printf("IsNotFibo\n")

NUM_TYPE all_fibs[SIZE_FIB_ARR];

// initialize all_fib array with all the fibonaci numbers that are smaller than num
void init_fib(NUM_TYPE num) {
    NUM_TYPE a = 0;
    NUM_TYPE b = 1;
    NUM_TYPE index;
    
    all_fibs[0] = a;
    all_fibs[1] = b;
    index = 2;
    while(a + b < num) {
        b += a;
        a = b-a;
        all_fibs[index++] = b;
    }
}

int main() {

    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    
    INP_TYPE amount_of_input;
    NUM_TYPE number_input;
    INP_TYPE i;
    int j;
    int found;
    
    if(scanf(INP_SCANF, &amount_of_input) <= 0) {
        return 1; // scanf fail
    }
       
    
    // setting up all the numbers
    init_fib(SIZE_NUM);
    
    // reading them
    for(i=0; i<amount_of_input; i++) {    
        if(scanf(NUM_SCANF, &number_input) <= 0) {
            return 1; // scanf fail
        }
        
        found = 0;
        for(j=0; (j<SIZE_FIB_ARR) && ((number_input != all_fibs[j])); j++) { 
        }
        if(j==SIZE_FIB_ARR) {
            PRINT_NO;
        } else {
            PRINT_YES;
        }     
    }
    
    
    return 0;
}